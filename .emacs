; init.el --- Emacs configuration

;;===============================================
;;
;; Package installations
;;
;;===============================================

(let ((bootstrap-file (concat user-emacs-directory "straight/repos/straight.el/bootstrap.el"))
      (bootstrap-version 3))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(defvar myPackages
  '(
    ;; essentials
    use-package
    better-defaults
    multiple-cursors
    multi-scratch
    
    ;; themes
    zerodark-theme
    ))

(mapc #'(lambda (package)
    (straight-use-package package))
      myPackages)

;;===============================================
;;
;; Require and configure packages
;;
;;===============================================

(require 'use-package)

(require 'better-defaults)

(require 'multiple-cursors)
(global-set-key (kbd "C-s-l") 'mc/edit-lines)
(global-set-key (kbd "C-s-f") 'mc/mark-next-like-this)

(require 'multi-scratch)
(global-set-key (kbd "C-s-T") 'multi-scratch-new)

(load-theme 'zerodark t)
(zerodark-setup-modeline-format)

;;===============================================
;;
;; Key bindings
;;
;;===============================================

(global-set-key (kbd "C-s-S") 'term)

;;===============================================
;;
;; Some basic customizations
;;
;;===============================================

(setq inhibit-startup-message t)
(global-linum-mode t)
(setq linum-format "%4d")
(global-visual-line-mode t)
(global-hl-line-mode t)
(column-number-mode t)
(size-indication-mode t)
(setq use-dialog-box nil)
(set-language-environment "UTF-8")
(setq visible-bell nil)

(set-face-attribute 'default nil :font "Menlo-14" )
(set-frame-font "Menlo-14" nil t)
(setq-default line-spacing 2)

;; initial window
(setq initial-frame-alist
     '((width  . 96) ; character
       (height . 48) ; lines
       ))

